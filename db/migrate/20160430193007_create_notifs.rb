class CreateNotifs < ActiveRecord::Migration[5.0]
  def change
    create_table :notifs do |t|
      t.text :content
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
    add_index :notifs, [:user_id, :created_at]
  end
end
