Rails.application.routes.draw do


  get 'rooms/show'

  root                'static_pages#home'
  get    'help'    => 'static_pages#help'
  get    'notifications'    => 'notifications#show'
  get    'about'   => 'static_pages#about'
  get    'contact' => 'static_pages#contact'
  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  get    'conversations'=> 'conversations#show'
  get    'notifs'  => 'notifications#show'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  delete 'notifs'  => 'notifs#supprimer'
  post    'notifications'    => 'notifications#show'
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :notifs,          only: [:create, :destroy]

  mount ActionCable.server => '/cable'
end
