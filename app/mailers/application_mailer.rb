class ApplicationMailer < ActionMailer::Base
  default from: "medteck15@gmail.com"
  layout 'mailer'
end
