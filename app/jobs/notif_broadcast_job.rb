class NotifBroadcastJob < ApplicationJob
  queue_as :default

  def perform(notif)
    if current_user.id != notif.user_id
      ActionCable.server.broadcast 'notification_channel', notif: render_notif(notif)
    end
  end

 private
   def render_notif(notif)
     ApplicationController.renderer.render(partial: 'notifs/notif', locals: { notif: notif })
   end
end
