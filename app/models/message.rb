class Message < ApplicationRecord
  belongs_to :sender,  class_name:  "User",
                       foreign_key: "sender_id",
                       dependent:   :destroy
  validates :sender_id, presence: true

  after_create_commit { MessageBroadcastJob.perform_later self }

end