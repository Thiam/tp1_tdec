class Notif < ApplicationRecord
  belongs_to :user
  default_scope -> { order('created_at DESC') }
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 500 }
  after_create_commit { NotifBroadcastJob.perform_later self }
end
