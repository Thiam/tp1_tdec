App.conversation = App.cable.subscriptions.create "ConversationChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    if document.getElementById('current_user_id').value in [data['sender_id'], data['receiver_id']]
      if data['sender_id'] is document.getElementById('current_user_id').value
        $('#tchat-body').append "<span id=\"send-message\"><p id=\"message-s\">"+data['message']+"</p></span>"
      else
        $('#tchat-body').append "<span id=\"receive-message\"><p id=\"message-r\">"+data['message']+"</p></span>"
        document.getElementById('receiver_id').value = data['sender_id']
      $("#tchat-body").animate {
        scrollTop: "+=#{$("#send-message").offset().top}px"
      }, "fast"

  speak: (message, sender_id, recipient_id) ->
    @perform 'speak', message: message, sender_id: sender_id, receiver_id: recipient_id

$(document).on 'click', '[data-radio~=receiver_id]', (event) ->
  if document.getElementById('receiver_id').value is event.target.value
  else
    document.getElementById('receiver_id').value = event.target.id
    document.getElementById('tchat-body').innerHTML = ""
    document.getElementById('title-contact').innerHTML = event.target.value

$(document).on 'keypress', '[data-behavior~=conversation_speaker]', (event) ->
  if event.keyCode is 13
    sid = document.getElementById('current_user_id').value
    rid = document.getElementById('receiver_id').value
    App.conversation.speak event.target.value, sid, rid
    event.target.value = ''
    event.preventDefault()

$(document).on 'click', '[id~=butSendMessage]', (event) ->
  sid = document.getElementById('current_user_id').value
  rid = document.getElementById('receiver_id').value
  message = document.getElementById('conversation_speaker').value
  App.conversation.speak message, sid, rid
  document.getElementById('conversation_speaker').value = ''
  event.preventDefault()