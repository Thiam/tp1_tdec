App.notification = App.cable.subscriptions.create "NotificationChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    $('#notification').append data['notif']

  speak: (notif, user_id, current_id) ->
    @perform 'speak', notif: notif, user_id: user_id, current_id: current_id

$(document).on 'keypress', '[data-behavior~=notification_speaker]', (event) ->
  if event.keyCode is 13 # return = send
    sid = document.getElementById('user_id').value
    App.notification.speak event.target.value, sid
    event.target.value = ""
    event.preventDefault()


$(document).on 'click', '[id~=everybody]', (event) ->
  notif = document.getElementById("notification_speaker").value
  na = document.getElementById("everybody").name
  App.notification.speak notif, na, na
  event.target.value = ""
  event.preventDefault()
