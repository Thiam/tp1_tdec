# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class ConversationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "conversation_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak(data)
    ActionCable.server.broadcast 'conversation_channel', message: data['message'], sender_id: data['sender_id'], receiver_id: data['receiver_id']
    Message.create! content: data['message'], sender_id: data['sender_id'], receiver_id: data['receiver_id']
  end
end
