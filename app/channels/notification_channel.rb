# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class NotificationChannel < ApplicationCable::Channel
  def subscribed
    stream_from "notification_channel"
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def speak (data)
    if data['current_id'] == data['user_id']
      followers = User.find(data['current_id']).followers
      followers.each do |f|
        Notif.create! content: data['notif'], user_id: f.id
      end
    else
      Notif.create! content: data['notif'], user_id: data['user_id']
    end
  end
end
