class NotifsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy, :sendall]
  before_action :correct_user,   only: :destroy

  def create
    if notif_params[:user_id] == nil
      followers = current_user.followers
      followers.each do |f|
        Notif.create! content: notif_params[:content], user_id: f.id
      end
    end
  end

  def destroy
    @notifs.destroy
    flash[:success] = "Notification deleted"
    redirect_to request.referrer
  end

  def supprimer
    @notification= Notif.where(user_id:current_user)
    @notification.destroy_all

    flash[:success] = "Notifications supprimées"
    redirect_to request.referrer
  end

  private
  def correct_user
    @notifs = current_user.notifs.find_by(id: params[:id])
    redirect_to root_url if @notifs.nil?
  end

  def notif_params
    params.require(:content).permit(:user_id)
  end

end
