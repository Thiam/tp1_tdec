class ConversationsController < ApplicationController
  #before_action :authenticate_user!

  layout false

  def show
    @messages = Message.where(receiver_id: current_user.id)
  end

end