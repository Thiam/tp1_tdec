class MessagesController < ApplicationController

  def create
    m = current_user.messages.build(message_params)
    m.save
  end

  private

    def message_params
      params.require(:content).permit(:content, :sender_id)
    end
end
