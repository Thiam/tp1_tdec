class NotificationsController < ApplicationController

  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy

  def show
    @notifs = Notif.where(user_id:current_user)
  end


  def correct_user
    @notifs = current_user.notifs.find_by(id: params[:id])
    redirect_to root_url if @notifs.nil?
  end


end
